module type NDet = sig
  type nd_int
  val int : int -> nd_int
                     
  type nd_ilist
  val nil : nd_ilist
  val cons : nd_int -> nd_ilist -> nd_ilist
  val list : int list -> nd_ilist

  val pattern_match :
    nd_ilist ->
    (unit -> nd_ilist) ->               (* if nil *)
    (nd_int -> nd_ilist -> nd_ilist) -> (* if cons *)
    nd_ilist

  val foldr :
    (nd_int -> nd_ilist -> nd_ilist) ->
    nd_ilist -> nd_ilist -> nd_ilist
                        
  val affiche : nd_ilist -> unit

  (* operations for non-determinism *)
  (* Failure *)
  val fail : nd_ilist
  (* Binary choice *)
  val (|||) : nd_ilist -> nd_ilist -> nd_ilist
end

(* Implementation possible *)
module NDetL = struct
  type nd_int = int list
  let int x = [x]

  (* Fonctions auxiliaires *)
  let concatmap: ('a -> 'b list) -> 'a list -> 'b list =
    fun f l -> List.concat (List.map f l)

  let liftm2 : ('a -> 'b -> 'c) -> 'a list -> 'b list -> 'c list =
    fun f l1 l2 -> concatmap (fun x -> List.map (f x) l2) l1
  (* ******************** *)

  type nd_ilist = int list list
  let nil = [[]]
  let cons: nd_int -> nd_ilist -> nd_ilist =
    liftm2 (fun h t -> h::t)
  let list l = [l]

  let pattern_match =
    fun l f_nil f_cons ->
    concatmap
      (function
         [] -> f_nil ()
       | h::t -> f_cons (int h) [t])
      l

  let rec foldr: (nd_int -> nd_ilist -> nd_ilist) ->
                 nd_ilist -> nd_ilist -> nd_ilist =
    fun f z l ->
    let go = function
        [] -> z
      | h::t -> f (int h) (foldr f z [t])
    in
    concatmap go l

  let fail = []
  let (|||): nd_ilist -> nd_ilist -> nd_ilist = (@)

      
  let affiche ll =
    let print_list l =
      let open Printf in
      printf "[%s] " (String.concat ";" (List.map string_of_int l))
    in
    print_string "[ ";
    List.iter print_list ll;
    print_string " ]\n"

end

module Perm (S:NDet) = struct
  open S
  let rec insert x l =
    cons x l ||| pattern_match
                   l
                   (fun () -> fail)
                   (fun h t -> cons h (insert x t))

  let perm = foldr insert nil
end
                          
let () =
  let module M = Perm(NDetL) in
  let open M in
  let open NDetL in
  let l = perm (list [1;2;3;4;5]) in
  affiche l;
  print_int (List.length l)

module type NDetComm = sig
  include NDet
  val rId : (int list -> bool) -> nd_ilist -> nd_ilist
  val once : nd_ilist -> nd_ilist
end

module Sort(Nd:NDetComm) = struct
  open Nd
  include Perm(Nd)

  let rec sorted = function
      [] | [_] -> true
      | h1::h2::t -> h1 <= h2 && sorted (h2::t)

  let sort l = once (rId sorted (perm l))

  let tests = sort (list [3;1;4;1;5;9;2])
end

module NDetLComm = struct
  include NDetL
  let rId = List.filter
  let once = function
      [] -> []
    | h::_ -> [h]
end

let l =
  let module M = Sort(NDetLComm) in M.tests
