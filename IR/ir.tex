\documentclass{beamer}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{listings}
\lstset {language=C}
\usepackage{proof}


\usetheme{CambridgeUS}
  
\title{Formal verification of a realistic compiler}

\author{Olivier Martinot}
\date{02/04/2019}
\institute[]{Université Paris Diderot}

\begin{document}

\frame{\titlepage}

\begin{frame}{Motivations}
  When writing a program, we can assume there will be bugs.
  But even a correct program can produce unexpected results if it is wrongly translated into the target language !

  This can cause problems in critical software, like those running in aircrafts, (self-driving) cars, etc.
\end{frame}


\begin{frame}
  What can we do ? Tests are not enough, we need a machine-checked proof of our compiler.

  Proving a compiler is not a new idea : it has been done for the first time in 1967 by McCarthy and Painter.

  Machine-checked proof is not a new idea either,
  and proof assistants such as Coq can mechanically check proofs of assertions about programs.
\end{frame}


\begin{frame}{CompCert}
  \begin{itemize}
  \item CompCert project led by Xavier Leroy (born in 1968, famous french computer scientist)
  \item CompCert is a Coq verified compiler
  \end{itemize}
\end{frame}


\begin{frame}{Overview of CompCert}
  CompCert translates from Clight (a large subset of the C programming language)
  to PowerPC assembly code (low-level instructions run by the processors).


  The compilation process can be split in 2 main parts : the \textbf{front end} and the \textbf{back end}.
  Today we'll focus only on the front end (the part that deals with the source language Clight).
\end{frame}

\begin{frame}{8 intermediate languages $\&$ 14 passes}
  \includegraphics[height=6cm]{CompilationPassesFrontEnd.png}
\end{frame}

\begin{frame}{Semantics preservation}
  We are interested in semantics preservation : the compiled code is proved to behave exactly as specified
  by the semantics of the source C program.

  \begin{block}{Notations}
    \begin{itemize}
    \item S : a source program
    \item C : a compiled program
    \item B : an observable behavior
    \item S $\Downarrow$ B : the program S executes with observable behavior B
    \item C $\models Spec$ : (C $safe$) $\land$ ($\forall B, C \Downarrow B \implies Spec(B))$
    \end{itemize}
  \end{block}
  
\end{frame}


\begin{frame}{Notions of semantics preservation}
  \begin{itemize}
  \item $\forall B, S \Downarrow B \iff C \Downarrow B$
  \item $S \models Spec \implies C \models Spec$
  \end{itemize}
  From now on, we will write $S \approx C$ to express one of the preceding semantic preservation properties.
\end{frame}

\iffalse
\begin{frame}{Different approaches to trusted compilation}
  $Comp(S) \in \{OK(C), Error\}$
  
  \begin{block}{Verified compiler}
    A compiler is verified if we have a formal proof that :\newline
    $\forall S,C, Comp (S) = OK(C) \implies S \approx C$
  \end{block}
  
  \begin{block}{Translation validation}
    Validator as a function $Validate : source code \times compiled code \to boolean$ which verifies $S \approx C$ a posteriori.
    We must then give a formal proof of the property $\forall S,C, Validate(S,C) = \top \implies S \approx C$ to ensure that it is correct.    
  \end{block}
\end{frame}
\fi

\begin{frame}{Abstract syntax of Clight}
  \begin{itemize}
  \item Types : integers, array, pointer, function but \emph{not} struct, union, typedef
  \item Expressions : all C operators except those related to struct and union
  \item Statements : all structured control statements of C (conditional, loop, break, continue, return) but not structured statements are omitted (goto, switch)
  \item Variables : global and local \textbf{auto} (declared at the beginning of functions) are allowed
  \end{itemize}
  Notation : $e^{\tau}$ denotes the expression $e$ carrying type $\tau$
\end{frame}


\begin{frame}{Big step semantics}
  Deterministic semantics given by the following judgements :
  \begin{align*}
    G,E &\vdash a,M \Rightarrow^{\ell} loc,M' &&\text{(expression in $\ell$-value position)}\\
    G,E &\vdash a,M \Rightarrow v,M' &&\text{(expression in r-value position)}\\
    G,E &\vdash a^?,M \Rightarrow v,M' &&\text{(optional expression)}\\
    G,E &\vdash a^*,M \Rightarrow v^*,M' &&\text{(list of expressions)}\\
    G,E &\vdash s,M \Rightarrow out,M' &&\text{(statement)}\\
    G &\vdash f(v^*),M \Rightarrow v,M' &&\text{(function invocation)}\\
    &\vdash p \Rightarrow v &&\text{(program)}
  \end{align*}
\end{frame}

\begin{frame}
  \includegraphics[height=7cm]{ValuesOutcomesEnvironments.png}
\end{frame}

\begin{frame}{A few inference rules}

  % TODO : Rewrite inference rules in TeX
  \iffalse
  \infer{
    G,E \vdash (a_1^{\tau_1} op a_2^{\tau_2})^{\tau},M \Rightarrow v,M_2
  } {
    G,E \vdash a_1^{\tau_1},M \Rightarrow v_1,M_1
    & G,E \vdash a_1^{\tau_2},M_1 \Rightarrow v_2,M_2
    & eval\_binary\_operation(op,v_1,\tau_1,v_2,\tau_2) = v
  }
  \fi

  \includegraphics[height=3cm]{InferenceRules.png}
\end{frame}



\begin{frame}{A few inference rules}
  % TODO : Rewrite inference rules in TeX
  \includegraphics[height=4cm]{InferenceRules2.png}
\end{frame}



\begin{frame}{Overview of Cminor}
  Difference with Clight :
  \begin{itemize}
  \item Arithmetic operators are not overloaded ($+$ becomes $+_i$ or $+_f$, ...)
  \item Address computations are explicit ($a[i]$ becomes $load(int32, a +_i i *_i 4$)
  \item Only 4 control structures : if-then-else, infinite loops, block-exit and early returns
  \item Within function, variables do not reside in memory (impossibility to use the $\&$ operator)
  \end{itemize}
\end{frame}



\begin{frame}
  \begin{align*}
    G,sp,L &\vdash a,E,M \Rightarrow v,E',M' &&\text{(expressions)}\\
    G,sp,L &\vdash a^*,E,M \Rightarrow v*,E',M' &&\text{(expression lists)}\\
    G,sp &\vdash s,E,M \Rightarrow out,E',M' &&\text{(statements)}\\
    G &\vdash fn(v^*),M \Rightarrow v,M' &&\text{(function calls)}\\
    &\vdash prog \Rightarrow v &&\text{(programs)}
  \end{align*}
\end{frame}



\begin{frame}{Translation}
  3 tasks :
  \begin{itemize}
  \item Resolution of operator overloading $(+, -, ...)$ and type-dependant behaviors $(cast,...)$
  \item Translation of $while$, $do ... while$ and $for$ loops into infinite loops with blocks and early exits.
  \item Placement of Clight variables (local variables, sub-areas of the Cminor stack block for the current function, globally allocated memory area)
  \end{itemize}
\end{frame}

\begin{frame}
  Casts : $\mathcal{C}^{\tau}_{\sigma}(e)$ casts e from type $\sigma$ to type $\tau$

  Expressions in l-value position :
  \begin{align*}
    \mathcal{L}_{\gamma}(e_1[e_2]) &= \mathcal{R}_{\gamma}(e_1 + e_2)\\
    &...
  \end{align*}
\end{frame}

\begin{frame}
  Expressions in r-value position :
  \begin{align*}
    %\mathcal{R}_{\gamma}(x) &= x if \gamma(x) = \textbf{Local}\\
    %\mathcal{R}_{\gamma}(x^{\tau} = e^{\sigma}) &= x \textbf{=} \mathcal{C}^{\tau}_{\sigma}(\mathcal{R}_{\gamma}(e^{\sigma})) if \gamma(x) = \textbf{Local}\\
    \mathcal{R}_{\gamma}(e_1^{\tau} + e_2^{\sigma}) &= \mathcal{R}_{\gamma}(e_1^{\tau}) +_i \mathcal{R}_{\gamma}(e_2^{\sigma}) \text{ if } \tau \text{ and } \sigma \text{ are integer types}\\
    \mathcal{R}_{\gamma}(e_1^{\tau} + e_2^{\sigma}) &= \mathcal{R}_{\gamma}(\mathcal{C}^{double}_{\tau}(e_1)) +_f \mathcal{R}_{\gamma}(\mathcal{C}^{double}_{\sigma}(e_2)) \text{ if } \tau \text{ and } \sigma \text{ are float types}\\
    \mathcal{R}_{\gamma}(e_1^{\tau} + e_2^{\sigma}) &= \mathcal{R}_{\gamma}(e_1^{\tau}) +_i \mathcal{R}_{\gamma}(e_2^{\sigma}) * _i \textbf{sizeof}(\rho) \text{ if } \tau \text{ is a pointer or array of } \rho\\
    &...
  \end{align*}
\end{frame}



\begin{frame}
  Statements :
  \begin{align*}
    \mathcal{S}_{\gamma}(\textbf{while}(e) s) &= block\{ loop\{ if(!\mathcal{R}_{\gamma}(e)) exit 0; block\{ \mathcal{S}_{\gamma}(s) \}\}\}\\
    \mathcal{S}_{\gamma}(\textbf{do} s \textbf{while}(e)) &= block\{ loop\{ block\{ \mathcal{S}_{\gamma}(s) \}; if(!\mathcal{R}_{\gamma}(e)) exit 0\}\}\\    
    \mathcal{S}_{\gamma}(\textbf{for}(e1;e2;e3) s) &= \mathcal{R}_{\gamma}(e_1);\\    
    &block\{ loop\{ if(!\mathcal{R}_{\gamma}(e_2)) exit 0;\\
    &block\{ \mathcal{S}_{\gamma}(s) \};\\
    &\mathcal{R}_{\gamma}(e_3) \}\}\\
    \mathcal{S}_{\gamma}(\textbf{break}) &= \textbf{exit} 1\\
    \mathcal{S}_{\gamma}(\textbf{continue}) &= \textbf{exit} 0
  \end{align*}
\end{frame}

\begin{frame}{Relating Memory States and Execution Environments}
  Relate the memory states and execution environments of the execution of a Clight code and that of the generated Cminor code.
\end{frame}



\begin{frame}{Proof by Simulation}
  The proof of semantic preservation for the translation is an induction over the Clight evaluation derivation and case analysis on the last evaluation rule used

  \begin{theorem}
    Assume the Clight program $p$ is well-typed and translates without errors to a Cminor program $p'$.
    If $p$ produces a value $v$, and if $v$ is an integer or a float value, then $p'$ produces the value $v$.
  \end{theorem}
\end{frame}



\begin{frame}{Technical side}
  The whole proof represents $\simeq$ 6000 lines of code.

  The source code of the translation represents $\simeq$ 800 lines of code.
\end{frame}



\begin{frame}{Performance}
  \includegraphics[height=4cm]{Perf.png}
\end{frame}



\begin{frame}{Future work}
  We could imagine that the compiler might handle a larger subset of C (struct and union), implement more optimizations, target other processors than PowerPC, etc.
\end{frame}

\begin{frame}{Bibliography}
  \begin{itemize}
  \item Xavier Leroy, \emph{Formal verification of a realistic compiler}
  \item Sandrine Blazy, Zaynah Dargaye, and Xavier Leroy, \emph{Formal Verification of a C Compiler Front-end}
  \end{itemize}
\end{frame}

\end{document}
