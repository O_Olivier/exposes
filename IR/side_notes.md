# Introduction 

## Motivations
When writing a program, we can assume there will be bugs.
But even a correct program can produce unexpected results if it is wrongly translated into machine code !
Indeed, compilers are complex and big programs that can introduce incorrect translations, especially when the compiler contains subtle optimizations.

Yet, we aren't forced to have a blind faith in compilers : we can apply formal methods to the compiler in order to prove the semantic preservation of source programs.
That's what Xavier Leroy and his team tried to do with the CompCert compiler.

## CompCert
   - CompCert is a Coq verified compiler
   - Clight is a large and realistic subset of C
   - Front-end : Clight $\to$ Cminor [4] | Back-end : Cminor $\to$ PowerPC assembly code [11,13]
   - 8 intermediate languages
   - Front-end : deals with the source language (parsing, ...)
   - Back-end : deals with the target language (producing assembly code, ...)
   - A few key references : [16] | [17] | [8]
   - Reference about typing : [18]
	
## Notions of semantic preservation
What do we mean by semantic preservation ?

Let S be a source program and C a compiled program.
We introduce a notion of observable behavior, which represent what the outside world can observe from the execution of the program. We write S $\Downarrow$ B if the program S executes with observable behavior B.

We write C $\models Spec$ if (C $safe$) $\land$ ($\forall B, C \Downarrow B \implies Spec(B))$ (we view $Spec$ as a predicate on the observable behaviors).

We have different notions of semantic preservation, more or less strong :

   - $\forall B, S \Downarrow B \iff C \Downarrow B$
   - $\forall B \notin Wrong, S \Downarrow B \implies C \Downarrow B$
   - $S \models Spec \implies C \models Spec$

From now on, we will write $S \approx C$ to express one of the preceding semantic preservation properties.

## Different approaches to trusted compilation
We can view a compiler as a total function $Comp$ from source program to either a compiled code ($OK(C)$) or a compile-time error ($Error$)

### Verified compiler
A compiler is verified if we have a formal proof that : $\forall S,C, Comp (S) = OK(C) \implies S \approx C$.

We can already notice that if the compiler always fail, it is verified !
Indeed, what we are interested in is not that the compiler is able to translate every well written program, but that the compiler never silently produces incorrect code.

### Translation validation
In this approach, the compiler is accompagnied with a validator : a boolean-valued function $Validate(S,C)$ that verifies the property $S \approx C$ \emph{a posteriori}.

If we have $Comp(S) = C$ and $Validate(S,C) = \top$, then the compiled code C must be valid.

The validation can be achieved by methods such as static analysis of $S$ and $C$, generation of conditions followed by model checking, ...

But can we trust our validator ?
We must verify it, with a formal proof of the property :
$\forall S,C, Validate(S,C) = \top \implies S \approx C$

Compilation can be decomposed into several passes. We need to prove each pass to certify the whole compiler. For each pass we have the choice between proving the code that implement this pass or perform the translation and verifying its results using a verified validator

## Formalisation of Clight

### Abstract syntax
   - Types : all integer types, array, pointer, function. _struct_, _union_, _typedef_ types are currently omitted.

   - Expressions : all C operators are supported, except those related to structs and unions. Notations : we write $e^{\tau}$ for the expression $e$ carrying type $\tau$. These types are necessary to determine semantics of overloaded arithmetic operators for instance

   - Statements : all structured control statements of C (conditional, loops, break, continue, return) are supported. Not structured statements are omitted (goto, switch).

   - Variables : global and local _auto_ variables (declared at the beginning of a function) are allowed, whereas block-scoped local variables and static variables are omitted.

A Clight program is a list of function definitions, a list of global variable declarations, and an identifier for the entry point of the program (main).

### Dynamic Semantics
C semantics are not deterministic (evaluation order for expression) but the semantics of Clight are (left-to-right evaluation)

Using big-step operational semantics :

   - $G,E \vdash a,M \Rightarrow^{\ell} loc,M'$ (expression in $\ell$-value position)
   - $G,E \vdash a,M \Rightarrow v,M'$ (expression in r-value position)
   - $G,E \vdash a^?,M \Rightarrow v,M'$ (optional expression)
   - $G,E \vdash a^*,M \Rightarrow v^*,M'$ (list of expressions)
   - $G,E \vdash s,M \Rightarrow out,M'$ (statement)
   - $G \vdash f(v^*),M \Rightarrow v,M'$ (function invocation)
   - $\vdash p \Rightarrow v$ (programs)

### The Memory Model of the Semantics
The memory model used in the dynamic semantics is described in [1].

Memory state M : reference $\to$ block contents.

A block has a low bound $L(M,b)$ and a high bound $H(M,b)$.

4 basic operations on memory are provided :

   - $alloc(M,lo,hi) = (M',b)$
   - $free(M,b) = M'$
   - $load(\kappa,M,b,n) = \lfloor v \rfloor$
   - $store(\kappa,M,b,n,v) = \lfloor M' \rfloor$
   
### Static Semantics (Typing rules)
Function from AST without type annotations to AST with type annotations

## Translation from Clight to Cminor

### Overview of Cminor
   - Clight : source language
   - Cminor : target language of the front-end compiler
	 and source language of the back-end compiler
	 
Cminor sematics can be expressed using the following inference rules :

   - $G,sp,L \vdash a,E,M \to v,E',M'$ (expressions)
   - $G,sp,L \vdash a^*,E,M \to v*,E',M'$ (expression lists)
   - $G,sp \vdash s,E,M \to out,E',M'$ (statements)
   - $G \vdash fn(v^*),M \to v,M'$ (function calls)
   - $\vdash prog \to v$ (programs)
   
Difference with Clight : local environment $E$ maps local variables to their values, and not their memory addresses, thus E is modified during evaluation of expressions and statements.

$sp$ is the reference to the stack block for the current function and $L$ is the environment from variables _let-bound_ within expressions.

### Overview of the translation
   - Resolution of operator overloading _(+, -, ...)_ and type-dependant behaviors _(cast,...)_
   - Translation of _while_, _do ... while_ and _for_ loops into infinite loops with blocks and early exits.
   - Placement of Clight variables (local variables, sub-areas of the Cminor stack block for the current function, globally allocated memory area)
   
$\rho$ maps variables to either **Local**, **Global** or **Stack($\delta$)** where $\delta$ a stack offset

Casts : $\mathcal{C}^{\tau}_{\sigma}(e)$ casts e from type $\sigma$ to type $\tau$

Expressions in l-value position :

   - $\mathcal{L}_{\gamma}(*e) =
   \textbf{addrglobal}(x) if \gamma(x) = \textbf{Global}$
   - $\mathcal{L}_{\gamma}(e_1[e_2]) =
   \mathcal{R}_{\gamma}(e_1 + e_2)$
   - ...

Expressions in r-value position :

   - $\mathcal{R}_{\gamma}(x) =
   x \text{ if } \gamma(x) = \textbf{Local}$
   - $\mathcal{R}_{\gamma}(x^{\tau} = e^{\sigma}) =
   x \textbf{=} \mathcal{C}^{\tau}_{\sigma}(\mathcal{R}_{\gamma}(e^{\sigma})) \text{ if } \gamma(x) = \textbf{Local}$
   - $\mathcal{R}_{\gamma}(e_1^{\tau} + e_2^{\sigma}) =
   \mathcal{R}_{\gamma}(e_1^{\tau}) +_i \mathcal{R}_{\gamma}(e_2^{\sigma}) \text{ if } \tau \text{ and } \sigma \text{ are integer types}$
   - $\mathcal{R}_{\gamma}(e_1^{\tau} + e_2^{\sigma}) =
   \mathcal{R}_{\gamma}(\mathcal{C}^{double}_{\tau}(e_1)) +_f \mathcal{R}_{\gamma}(\mathcal{C}^{double}_{\sigma}(e_2))$
   - $\mathcal{R}_{\gamma}(e_1^{\tau} + e_2^{\sigma}) =
   \mathcal{R}_{\gamma}(e_1^{\tau}) +_i \mathcal{R}_{\gamma}(e_2^{\sigma}) * _i \textbf{sizeof}(\rho) \text{ if } \tau \text{ is a pointer or array of } \rho$
   - ...
   
Statements :

   - $\mathcal{S}_{\gamma}(\textbf{while}(e) s) = block\{ loop\{ if(!\mathcal{R}_{\gamma}(e)) exit 0; block\{\mathcal{S}_{\gamma}(s) \}\}\}$
   - $\mathcal{S}_{\gamma}(\textbf{do} s \textbf{while}(e)) = block\{ loop\{ block\{ \mathcal{S}_{\gamma}(s) \}; if(!\mathcal{R}_{\gamma}(e)) exit 0\}\}$
   - $\mathcal{S}_{\gamma}(\textbf{for}(e1;e2;e3) s) = \mathcal{R}_{\gamma}(e_1); block\{ loop\{ if(!\mathcal{R}_{\gamma}(e_2)) exit 0; block\{ \mathcal{S}_{\gamma}(s) \}; \mathcal{R}_{\gamma}(e_3) \}\}$
   - $\mathcal{S}_{\gamma}(\textbf{break}) = \textbf{exit} 1$
   - $\mathcal{S}_{\gamma}(\textbf{continue}) = \textbf{exit} 0$
   
## Proof of correctness

### Relating Memory States
Problem : relate the memory states of the execution of a Clight code and that of the generated Cminor code.

   - Idea : Clight block $b$ corresponds to Cminor block $b'$ with offset $\delta$
   - Notion of memory injection $\alpha(b) \in \{ \textbf{None}, \lfloor b',\delta \rfloor \}$
   
Relation between Clight values and Cminor values ($\alpha \vdash v \approx v'$) :

   - $\alpha \vdash Vint(n) \approx Vint(n)$
   - $\alpha \vdash Vfloat(n) \approx Vfloat(n)$
   - $\alpha \vdash Vundef \approx v$
   - if $\alpha(b) = \lfloor b',\delta \rfloor$ and $i' = i + \delta$ (mod $2^{32}$) then $\alpha \vdash Vptr(b,i) \approx Vptr(b',i')$
   
### Relating Execution Environment
In Clight : maps variables to references of blocks containing the values of the variables.
In Cminor : maps variables directly to their values.
We define a relation $EnvMatch(\gamma,\alpha,E,M,E',sp)$ between Clight environment $E$ and memory state $M$ and a Cminor environment $E'$ and a reference to a stack block $sp$

### Proof by Simulation

### Technical side

### Future work
  We could imagine that the compiler might handle a larger subset of C, implement more optimizations, target other processors than PowerPC, etc.

## Bibliography
   - Xavier Leroy, _Formal verification of a realistic compiler_
   - Sandrine Blazy, Zaynah Dargaye, and Xavier Leroy, _Formal Verification of a C Compiler Front-end_
