RM_AUX = rm -f $@.aux $@.log $@.nav $@.out $@.snm $@.toc $@.vrb
TEX2PDF = pdflatex $@.tex
EXEC = $(TEX2PDF) && $(RM_AUX)


all:stream non_det gödel topos ir

stream: StreamPresentation/
	@cd $^Francais && $(EXEC) && cd ../../$^English && $(EXEC)

non_det: NonDeterministicComputation/
	cd $^ && $(EXEC)

gödel: Gödel/
	cd $^ && $(EXEC)
topos: Topos/
	cd $^ && pandoc -t latex -o ecologie.pdf ecologie.md

ir: IR/
	cd $^ && $(EXEC)

clean:
	rm -f */*~ */*/*~
