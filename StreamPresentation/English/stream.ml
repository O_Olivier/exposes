open List

let accumulate_interval a b =
  let rec aux acc i =
    if i > b then acc
    else aux (i::acc) (i+1)
  in List.rev (aux [] a)

let interval = accumulate_interval

(* Not implemented *)
let prime a = true

       
let deuxieme l = nth l 2

let f a b = deuxieme
              (filter prime
                      (accumulate_interval a b))

let f a b =
  let acc = accumulate_interval a b in
  let filtre_prime = filter prime acc in
  match filtre_prime with
  | x::y::t -> y
  | _ -> raise (invalid_arg "intervalle invalide")

let f a b =
  let rec aux i compteur =
    if i<=b then
      match prime i with
      | false -> aux (i+1) compteur
      | true -> match compteur with
                | 1 -> i
                | _ -> aux (i+1) (compteur+1)
    else raise (invalid_arg "intervalle invalide")
  in aux a 0

type 'a stream = Nil | Cons of 'a * (unit -> 'a stream)

let empty_stream = Nil
                     
let singleton n = Cons (n, fun () -> Nil)
                       
let deux = singleton 2

let un_deux = Cons (1, fun () -> deux)

let hd s =
  match s with
    Nil -> failwith "hd"
  | Cons (x,_) -> x

let tl s =
  match s with
    Nil -> failwith "tl"
  | Cons (_,f) -> f ()
                    
let rec ones = 1::ones
                 
let rec ones = Cons (1, fun () -> ones)
                    
let rec starting_from n =
  Cons (n, fun () -> starting_from (n + 1))

let naturals = starting_from 0

let rec nth s n =
  match n with
    0 -> hd s
  | _ -> nth (tl s) (n - 1)

let from_list lst =
  List.fold_right
    (fun x s -> Cons (x, fun () -> s)) lst Nil
    
(* make a list from the 
 * first n elements of a stream *)
let rec take s n =
  if n <= 0 then [] else
  match s with
    Nil -> []
  | _ -> hd s :: take (tl s) (n - 1)

let rec map f s =
  match s with
    Nil -> Nil
  | Cons(x,g) -> Cons(f x,
                      fun () -> map f (g ()))

let rec filter pred s =
  match s with
    Nil -> Nil
  | Cons(x,g) ->
     if pred x then
       Cons(x,
            fun () -> filter pred (g ()))
     else
       filter pred (g ())

let div_7 n = (n mod 7 = 0)

let s = filter div_7 naturals

let l = take s 5

let square n = n * n

let fib =
  let rec fib_aux a b =
    Cons(a, fun () -> fib_aux b (a+b))
  in fib_aux 0 1

let filtre_mult p =
  filter (fun n -> n mod p <> 0)
         
let rec sieve s =
  match s with
    Nil -> Nil
  | Cons(p, f) ->
     Cons(p,
          fun () -> sieve (filtre_mult p (f ())))

let prime = sieve (starting_from 2)

let rec ones = Cons (1, fun () -> ones)
