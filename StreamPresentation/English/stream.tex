\documentclass{beamer}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
%\usepackage[francais]{babel}

%souligner plusieurs lignes
\usepackage{ulem}

%ecrire du code
\usepackage{listings}

%ecrire des intervalles d'entiers
\usepackage{stmaryrd}

\usetheme{CambridgeUS}

\title{Streams}
\lstset{
 language=Caml, 
 commentstyle=\color{gray}, 
 basicstyle=\small, 
 keywordstyle=\small\bfseries, 
 %numbers=left, 
 %stepnumber=2, 
 numberblanklines=false, 
 %frame=single, 
 captionpos=b, 
 title=\lstname, 
 inputpath=src, 
 escapeinside={//@}{@}, 
 abovecaptionskip=0pt, 
 numberstyle=\tiny,
 numbersep=6pt
}


\begin{document}

\frame{\titlepage}

\begin{frame}
  \begin{quotation}
    Streams are a clever idea that allows one to use sequence manipulations without incurring the costs of manipulating sequences as lists.
  \end{quotation}
  \rightline{Abelson and Sussman}
\end{frame}

\frame{\tableofcontents[sections={1-3}]}





\section{Motivations}




\begin{frame}
  \begin{block}{Example of problem :}
    One wants to find the second prime number in an interval
  \end{block}
  For instance :
  \begin{itemize}
    \item For $\mathbb{N}$ : [ 0; 1 ; 2 ; \boxed{3} ; ... ]
    \item For the interval $\llbracket 10; 13; 16;  ... \rrbracket$ : [ 10; 13; 16; \boxed{19} ]
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Sequence manipulation}
  \begin{block}{Steps :}
    \begin{enumerate}
    \item enumerate the interval
    \item filter the prime numbers
    \item take the second one
    \end{enumerate}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \begin{lstlisting}
    # let f a b = second
                        (filter prime
                                     (interval a b))
    val f : int -> int -> int = <fun>
  \end{lstlisting}
\end{frame}




\begin{frame}[fragile]{Incremental approach}
  \begin{lstlisting}
    # let f a b =
        let rec aux i count =
          if i<=b then
            match prime i with
            | false -> aux (i+1) count
            | true -> if count = 1 then i
                      else aux (i+1) (count+1)
          else raise (invalid_arg "invalid interval")
        in aux a 0
    val f : int -> int -> int = <fun>          
  \end{lstlisting}
\end{frame}


\begin{frame}{Sequence manipulation VS incremental approach}

  \begin{block}{Pros of sequence manipulation}
    \begin{itemize}
    \item Elegance
    \item Abstraction
    \end{itemize}
  \end{block}

  \pause

  \begin{block}{Pros of incremental approach}
    \begin{itemize}
    \item Efficiency (time and space)
    \end{itemize}
  \end{block}

\end{frame}






\section{Building streams}






\begin{frame}{Best of both worlds : the streams !}
  \begin{block}{Idea}
    \begin{itemize}
    \item construct a stream only partially
    \item pass the partial construction to the program that consumes the stream
    \item if the consumer attempts to access a part of the stream that has not yet
      been constructed, the stream will construct just enough more of itself to produce the required
      part
      % Lazy construction
      % Illusion of an already constructed data structure
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}[fragile]{Lazy evaluation \& Thunks}
  \begin{block}{Thunks}
    In Ocaml, evaluation isn't lazy. We need to emulate laziness with a \emph{thunk} : a fonction with the form ``\emph{fun () -> ...}''
    % Thus the body of the function is encapsulate and isn't evaluate at definition time but at application time
  \end{block}

  \begin{lstlisting}
    Exemple :
    # let f = fun () -> print_string "Hello\n";;
    val f : unit -> unit = <fun>
    # f ();;
    Hello
    - : unit = ()
  \end{lstlisting}

\end{frame}

\begin{frame}{What is a stream ?}
  \begin{block}{Classic lists :}
    type 'a list = Nil | Cons of 'a * 'a list
  \end{block}
  
  \begin{block}{Streams}
    type 'a stream = Nil | Cons of 'a * (unit ->  'a  stream) \linebreak
    We delay the creation of the tail with a thunk.\linebreak
    In fact, a (non empty) stream = one element + a way to evaluate the rest of the stream
  \end{block}
\end{frame}


\begin{frame}[fragile]{Examples of streams}
  \begin{lstlisting}
    let empty_stream = Nil
    
    let singleton n = Cons (n, fun () -> empty_stream)
    
    let two = singleton 2

    let one_two = Cons (1, fun () -> two)
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Operations on streams}
  \begin{lstlisting}
    # let hd s =
        match s with
        | Nil -> failwith "hd"
        | Cons (x,_) -> x
    val hd : 'a stream -> 'a = <fun>


    # let tl s =
        match s with
        | Nil -> failwith "tl"
        | Cons (_,f) -> f ()
    val tl : 'a stream -> 'a stream = <fun>
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \begin{lstlisting}
    # one_two;;
    - : int stream = Cons (1, <fun>)
    # hd one_two;;
    - : int = 1
    # tl one_two;;
    - : int stream = Cons (2, <fun>)
  \end{lstlisting}
\end{frame}



\section{Infinite stream}





\begin{frame}[fragile]{Infinite streams, or why functional programming is great}
  \begin{block}{Infinite data structure in a finite space}
    We can also define infinite streams !
  \end{block}
  \begin{lstlisting}
    # let rec ones = Cons (1, fun () -> ones);;
    val ones : int stream = Cons (1, <fun>)

    # let rec starting_from n =
        Cons (n, fun () -> starting_from (n + 1));;
    val starting_from : int -> int stream = <fun>

    # let naturals = starting_from 0;;
    val naturals : int stream = Cons (0, <fun>)
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Stream manipulation}
  \begin{lstlisting}
    
   # let rec map f s =
       match s with
       | Nil -> Nil
       | Cons(x,g) -> Cons(f x,
                           fun () -> map f (g ()))
   val map : ('a -> 'b) -> 'a stream -> 'b stream = <fun>
                      
   # let rec filter pred s =
       match s with
       | Nil -> Nil
       | Cons(x,g) ->
           if pred x then
             Cons(x, fun () -> filter pred (g ()))
           else
             filter pred (g ())
   val filter: ('a -> bool)->'a stream->'a stream = <fun>
  \end{lstlisting}
\end{frame}

\begin{frame}
  \begin{block}{Remark}
    This stream implementation isn't very efficient, because it doesn't keep already computed elements in memory. We can deal with this problem using memoization.
  \end{block}
\end{frame}





\section{Sources}





\begin{frame}{Sources}
  \begin{block}{DIY stream implementation}
    \begin{itemize}
    \item \textit{Structure and Interpretation of Computer Programs} (known as \textit{the Wizard Book}) by Abelson, Sussman \& Sussman : \underline{https://mitpress.mit.edu/sicp/full-text/book/book.html}
    \item Ralf Treinen's course \textit{Advanced functional programming} : \underline{https://www.irif.fr/~treinen/teaching/pfav/}
    \item The course from \underline{Cornell University} :
      \uline{http://www.cs.cornell.edu/courses/cs3110/2011fa/\linebreak supplemental/lec24-streams/streams.htm}
    \end{itemize}
  \end{block}
\end{frame}


\end{document}
